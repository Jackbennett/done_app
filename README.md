# Done App

## Vandaag Gedaan, Dutch for Done Today

Like a todo app, but past tense.

Note what you've done today, it will be nice to reflect on later or to use when planning your ToDo list tomorrow.

- Add a title, description and a maybe a photo of what you've done. (app records time/date)
- View past done entries
- Tag done items with anything
- See an overview of things done every day.

The idea for this is personal goals and activities, not performance tracking.

# App

## Frontend

Vue 3 + tailwind CSS + typescript

### Features

- [x] Add done items as a list
- [ ] Persistent storage
- [ ] Add images to items
- [ ] Graph done items over time
- [ ] Sort by tag
- [ ] Graph done items by tag

### todo (maybe)

- Edit items title/description after the fact
- Choose date when entering items for back-dating

## Backend TODO

Currently working to use [PostgREST](http://postgrest.org/en/v7.0.0/index.html) as the api to postgres as the data schema should be static for all app isntances.

This is in my own k3s VM as a service in front of postgres also running in kubernetes.

### Experimental:

[Cayley Graph DB](https://cayley.io/) backed by leveldb <=> graphQL to frontend

Never tried a graph db, never tried graphQL so that would be good to have a go. The two aren't required to be used together.

Another option might just be CouchDB + PouchDB

- [ ] Cross-device storage
