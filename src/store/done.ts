import { PersistentStore } from './_base'
import { DONE_STORE_NAME } from './_names'
import { Done, DoneList } from './interface'

class DoneStore extends PersistentStore<DoneList> {
  protected data(): DoneList {
    return {
      tags: new Set(),
      list: [],
    }
  }

  add(done: Done): void {
    if (done?.tags) {
      for (let tag of done.tags) {
        this.state.tags.add(tag)
      }
    }
    this.state.list.push(done)
  }
}

export const doneStore = new DoneStore(DONE_STORE_NAME)
